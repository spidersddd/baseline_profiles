# frozen_string_literal: true

require 'spec_helper'

describe 'baseline_profiles::linux::environment::variable' do
  let(:title) { 'namevar' }
  let(:params) do
    { value: 'somevalue', }
  end

  on_supported_os.each do |os, os_facts|
    next if os_facts[:os]['family'] == 'windows'
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
