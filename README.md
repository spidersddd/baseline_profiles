# baseline_profiles

# Puppet Example Baseline Profiles

This directory is working example profiles for customers to use as well as model
future development after.  They should follow best practice and model a multi OS 
use case.

It SHOULD go without saying that everything should pass linting/validation, but
we're gonna go ahead and say that anyway.

## Organization and Namespaces

Profile sprawl is a real concern. To prevent that, please try to follow a convention.

Profiles should be organized in a hierarchical form with a few top-level categories that descend into more specific things. 

### The Baseline Profiles

`./manifests/`

  * This module should contain profiles that manage items built-in to an operating system. For example, DNS, NTP, Users, firewall rules, etc....
  * If the thing being managed is "out of the box", it goes here.

`./manifests/baseline.pp`

  * Profile that will wrap the OS level profiles 
  * Profile that build to support all business supported OS.
  * Profile to implement the minimium base that security, product and sysadmins will allow on a company network.
  * Profile built to install the base software for a company that is identified as site wide software, ie backup etc.

`./manifests/<os_family>/<os_name>/<what it is for>`

  * Profiles that are specific to built-in settings of one operating system go here.
  * For example, `/manifests/windows/security.pp`, `/manifests/linux/firewall.pp`, or `/manifests/solaris/enable_ssh.pp`.

`./lib/facter/`

  * This will be used a location to store site (company) specific facts not specific to a component module.

`./functions/`

  * This directory is for site specific functions not related to modules.

