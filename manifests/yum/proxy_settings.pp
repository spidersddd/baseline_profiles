# profile to set proxy settings in yum.conf
class baseline_profiles::yum::proxy_settings (
  String $proxy = 'http://192.168.5.6:3128',
) {
  yum::config { 'proxy':
    ensure => $proxy,
  }
}
