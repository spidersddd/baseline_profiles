# Profile to run on all hosts.
# This profile is meant to be ran on ALL
# supported OS types.
class baseline_profiles::baseline (
  Enum['enforcing', 'permissive', 'disabled'] $selinux_mode = 'enforcing',
) {
  case $facts['os']['family'] {
    'RedHat': {
      require baseline_profiles::yum::proxy_settings
      include baseline_profiles::linux::ssh
      include baseline_profiles::linux::centos::repos::centos_release_scl_rh
      class { 'baseline_profiles::selinux':
        mode => $selinux_mode,
      }
    }
    'Debian': {
      include baseline_profiles::linux::ssh
    }
    'windows': {
      notify{ 'windows_messsage_note':
        message  => 'Nothing being added by baseline_profiles::baseline',
      }
    }
    default: {
      # this should be some sort of failure
    }
  }

  include baseline_profiles::hosts
}
