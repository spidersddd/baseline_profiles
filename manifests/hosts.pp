# Class for managing export hosts resources
class baseline_profiles::hosts (
) {
  @@host { $facts['fqdn']:
    ensure       => present,
    ip           => $facts['networking']['ip'],
    host_aliases => [ $facts['hostname'] ],
  }
  # Collect:
  Host <<| |>>
}
