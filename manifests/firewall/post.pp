# profile to drop all traffic after listed rules
class baseline_profiles::firewall::post {
  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }
}
