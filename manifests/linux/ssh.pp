# Profile to setup exported resources of ssh keys
class baseline_profiles::linux::ssh (
    Boolean $storeconfigs_enabled = false,
) {
  include baseline_profiles::firewall

  class { 'ssh':
    storeconfigs_enabled => $storeconfigs_enabled,
  }

  file { '/etc/ssh/ssh_known_hosts':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  if $facts['ssh']['rsa'] {
    @@sshkey { "${facts['fqdn']}_rsa":
      ensure => present,
      key    => $facts['ssh']['rsa']['key'],
      type   => rsa,
      tag    => $facts['fqdn'],
    }
  }

  if $facts['ssh']['ecdsa'] {
    @@sshkey { "${facts['fqdn']}_ecdsa":
      ensure => present,
      key    => $facts['ssh']['ecdsa']['key'],
      type   => dsa,
      tag    => $facts['fqdn'],
    }
  }

  if $facts['ssh']['ed25519'] {
    @@sshkey { "${facts['fqdn']}_ed25519":
      ensure => present,
      key    => $facts['ssh']['ed25519']['key'],
      type   => ed25519,
      tag    => $facts['fqdn'],
    }
  }

  firewall { '101 baseline_profiles::linux::ssh rules':
    proto  => 'tcp',
    action => 'accept',
    dport  => '22',
  }

  Sshkey <<| |>>

}
