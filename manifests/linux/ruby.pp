# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include baseline_profiles::linux::ruby
class baseline_profiles::linux::ruby {
  case $facts['os']['family'] {
    'RedHat': {
      include baseline_profiles::linux::centos::rh_ruby
    }
    'Debian': {
      include baseline_profiles::linux::debian::ruby
    }
    default: {
      # this should be some sort of failure
    }
  }

}
