# @summary A defined resource to manage environment variables.
#
# This will manage r
#
# @example
#   baseline_profiles::linux::environment::variable { 'namevar': 
#     value => 'someSillyValue',
#   }
define baseline_profiles::linux::environment::variable (
  String $value,
  String $variable_name = $title,
  Enum['absent', 'present'] $ensure = 'present',
) {
  file_line { "setting ${variable_name}":
    ensure => $ensure,
    path   => '/etc/profile.d/sh.local',
    line   => "export ${variable_name}=${value}",
    match  => "export ${variable_name}=",
  }
}
