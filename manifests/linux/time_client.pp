# Profile to install and configure time clients
class baseline_profiles::linux::time_client (
  Array[String] $servers = [ '0.id.pool.ntp.org', '1.id.pool.ntp.org', '2.id.pool.ntp.org', '3.id.pool.ntp.org' ]
) {
  $server_hash = $servers.map |$v| { [ $v, [ 'iburst' ]] }.convert_to(Hash)

  include baseline_profiles::firewall

  class { '::chrony':
    servers => $server_hash,
  }
}
