# profile to install the scl RH repos
class baseline_profiles::linux::centos::repos::centos_release_scl_rh (
  $ensure = 'installed',
) {
  package { 'centos-release-scl-rh':
    ensure => $ensure,
  }
}
