# This will install and link ruby and gem in /usr/local/bin/
class baseline_profiles::linux::centos::rh_ruby (
  Boolean $create_links = false,
  Integer $mversion = 27,
) {

  include baseline_profiles::linux::centos::repos::centos_release_scl_rh

  package { [ "rh-ruby${mversion}", "rh-ruby${mversion}-rubygems" ]:
    ensure  => installed,
    require => Class['baseline_profiles::linux::centos::repos::centos_release_scl_rh'],
  }

  if $create_links {
    $new_bins = [ 'ruby', 'gem', ]
    $new_bins.each | String $bin | {
      file { "/usr/local/bin/${bin}":
        ensure => link,
        target => "/opt/rh/rh-ruby${mversion}/root/bin/${bin}",
      }
    }
  }
  baseline_profiles::linux::environment::variable { 'PATH':
    value => "\$PATH:/opt/puppetlabs/puppet/bin:/opt/rh/rh-ruby${mversion}/root/usr/local/bin:/opt/rh/rh-ruby${mversion}/root/usr/bin",
  }
  baseline_profiles::linux::environment::variable { 'LD_LIBRARY_PATH':
    value => "/opt/rh/rh-ruby${mversion}/root/usr/local/lib64:/opt/rh/rh-ruby${mversion}/root/usr/lib64:\$LD_LIBRARY_PATH", # lint:ignore:140chars
  }
  baseline_profiles::linux::environment::variable { 'MANPATH':
    value => "/opt/rh/rh-ruby${mversion}/root/usr/local/share/man:/opt/rh/rh-ruby${mversion}/root/usr/share/man:\$MANPATH"
  }
  baseline_profiles::linux::environment::variable { 'PKG_CONFIG_PATH':
    value => "/opt/rh/rh-ruby${mversion}/root/usr/local/lib64/pkgconfig:/opt/rh/rh-ruby${mversion}/root/usr/lib64/pkgconfig:\$PKG_CONFIG_PATH", # lint:ignore:140chars
  }
  baseline_profiles::linux::environment::variable { 'XDG_DATA_DIRS':
    value => "/opt/rh/rh-ruby${mversion}/root/usr/local/share:/opt/rh/rh-ruby${mversion}/root/usr/share:/usr/local/share:/usr/share", # lint:ignore:140chars
  }
  reboot { 'after':
    apply     => 'finished',
    message   => 'Rebooting to pull in new SHELL environment settings.',
    subscribe => Baseline_profiles::Linux::Environment::Variable[
      'PATH','LD_LIBRARY_PATH', 'MANPATH', 'PKG_CONFIG_PATH', 'XDG_DATA_DIRS',
    ],
  }
}
