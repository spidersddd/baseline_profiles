# @summary This will install ruby to the host
#
# A class to install ruby onto a debian system
#
# @example
#   include baseline_profiles::linux::debian::ruby
class baseline_profiles::linux::debian::ruby (
  Array $packages = [ 'ruby' ]
) {
  package { $packages:
    ensure => installed,
  }
}
