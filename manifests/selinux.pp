# profile to set selinux state
class baseline_profiles::selinux (
  Optional[Enum['enforcing', 'permissive', 'disabled']] $mode = 'enforcing',
) {
  class { 'selinux':
    mode => $mode,
  }
}
