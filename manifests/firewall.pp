# Profile to order the loading of firewall rules
class baseline_profiles::firewall {
  Firewall {
    before  => Class['baseline_profiles::firewall::post'],
    require => Class['baseline_profiles::firewall::pre'],
  }
  include firewall

  class { ['baseline_profiles::firewall::pre', 'baseline_profiles::firewall::post']: }
}
